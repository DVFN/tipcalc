package com.colegio.devin.tipcalc.fragments;

import com.colegio.devin.tipcalc.model.TipRecord;

/**
 * Created by devin on 19/06/2016.
 */
public interface TipHistoryListFragmentListener {
    void addToList(TipRecord record);
    void clearList();
}
