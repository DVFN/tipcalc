package com.colegio.devin.tipcalc.model;

/**
 * Created by devin on 19/06/2016.
 */
import java.text.SimpleDateFormat;
import java.util.Date;

public class TipRecord {
        private double bill;
        private int tipPercentage;
        private Date timeStamp;

    public int getTipPercentage() {
        return tipPercentage;
    }

    public void setTipPercentage(int tipPercentage) {
        this.tipPercentage = tipPercentage;
    }

    public double getBill() {
        return bill;
    }

    public void setBill(double bill) {
        this.bill = bill;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
    public double getTip(){
        return bill=(tipPercentage/100);
    }
    public String getDateFormatted(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        return simpleDateFormat.format(timeStamp);
    }
}
