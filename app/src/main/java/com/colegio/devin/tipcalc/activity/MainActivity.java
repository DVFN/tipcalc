package com.colegio.devin.tipcalc.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.colegio.devin.tipcalc.R;
import com.colegio.devin.tipcalc.activity.TipCalcClass;
import com.colegio.devin.tipcalc.fragments.TipHistoryListFragmentListener;
import com.colegio.devin.tipcalc.model.TipRecord;

import java.util.Date;

import butterknife.Bind;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.ingresarTotal)
    EditText inputBill;
    @Bind(R.id.button2)
    Button button2;
    @Bind(R.id.ingresarPorcentaje)
    EditText ingresarPorcentaje;
    @Bind(R.id.button)
    Button button;
    @Bind(R.id.incrementar)
    Button incrementar;
    @Bind(R.id.disminuir)
    Button disminuir;
    @Bind(R.id.tipContent)
    Button tipContent;
    @Bind(R.id.srpTotal)
    TextView srpTotal;

    private TipHistoryListFragmentListener fragmentListener;
    private final static int TIP_STEP_CHANGE = 1 ;
    private final static int DEFAULT_TIP_PERCENTAGE = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main , menu);
        return super.onCreateOptionsMenu(menu);

        TipHistoryListFragmentListener fragment= (TipHistoryListFragmentListener) getSupportFragmentManager().findFragmentById(R.id.fragmentList);
        fragment.setRetainInstance(true);

        fragmentListener = fragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_about){
            about();
        }
        return super.onOptionsItemSelected(item);
    }
    @OnClick(R.id.button)
    public void handleClickSubmit(){
        hideKeyboard();
        String strInputTotal = inputBill.getText().toString().trim();
        if(!strInputTotal.isEmpty()){
            double total = Double.parseDouble(strInputTotal);
            int tipPercentage = getTipPercentage();
            TipRecord tipRecord = new TipRecord();
            tipRecord.setBill(total);
            tipRecord.setTipPercentage(tipPercentage);
            tipRecord.setTimeStamp(new Date());
            String strTip = String.format(getString(R.string.global_mensaje_propina),tipRecord.getTip());
            fragmentListener.addToList(tipRecord);
            srpTotal.setVisibility(View.VISIBLE);
            srpTotal.setText(strTip);
        }
    }
    @OnClick(R.id.incrementar)
    public  void handleClickIncrease(){
        hideKeyboard();
        handleTipChange(TIP_STEP_CHANGE);
    }

    @OnClick(R.id.disminuir)
    public void handleClickDecrease(){
        hideKeyboard();
        handleTipChange(-TIP_STEP_CHANGE);
    }

    @OnClick(R.id.button2)
    public void handleClickClear(){
        hideKeyboard();
        fragmentListener.clearList();
    }
    public int getTipPercentage(){
        return DEFAULT_TIP_PERCENTAGE;
    }
    private void hideKeyboard(){
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
        }catch (NullPointerException npe){
            Log.e(getLocalClassName(),Log.getStackTraceString(npe));
        }
    }
    private void about(){
        TipCalcClass app = (TipCalcClass)getApplication();
        String strURL = TipCalcClass.getAboutUrl();
        Intent intent= new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(strURL));
        startActivity(intent);
    }
}